(function($, window){
	$(window).on('load', function() {
    var pisowifi = new Pisowifi();
    var my_distance = false;
    var my_turn = false;
    
    var coindrop = new Howl({
      src: [audio.coindrop.webm, audio.coindrop.mp3]
    });
    var addtime = new Howl({
      src: [audio.addtime.webm, audio.addtime.mp3]
    });
    
    var confirm_btn_check = function(){
      $('.btn-insert-coin').prop('disabled',!(my_distance && my_turn))
      .toggleClass('btn-secondary',!(my_distance && my_turn)).toggleClass('btn-success',(my_distance && my_turn));
    }
    
    pisowifi.on('wifi', function( data ){
      var connected = data.connected === true;
      $('.btn-connect').text(function(){
        return !connected ? 'Connect' : 'Disconnect';
      });
    })
    
    pisowifi.on('connect', function( ){
      
    })
    pisowifi.on('disconnect', function( reason ){
      
    })
    
    pisowifi.on('timer', function( time, status ){
      var _time = time({raw:true}) > 0;
      $('.btn-connect').prop('disabled',!_time)
        .toggleClass('btn-secondary',!_time).toggleClass('btn-success',_time);
      $('.timer').text( time() );
    })
    
    pisowifi.on('heartbeat', function( data ){
      my_distance = data.distance.max > data.distance.current;
      my_turn = data.myturn === true;
      if ( data.distance.current > 0 ) {
        $('.nearcoinbox').closest('li').removeClass('hide');
      }
      if ( my_distance ) {
        $('.nearcoinbox .fas').removeClass('fa-spinner fa-pulse').addClass('fa-check');
        $('.nearcoinbox span').text('passed');
      } else {
        $('.nearcoinbox .fas').addClass('fa-spinner fa-pulse').removeClass('fa-check');
        $('.nearcoinbox span').text('looking...');
      }
      if ( data.myturn === true ) {
        $('.myturn .fas').removeClass('fa-spinner fa-pulse').addClass('fa-check');
        $('.myturn span').text('passed');
      } else {
        $('.myturn .fas').addClass('fa-spinner fa-pulse').removeClass('fa-check');
        $('.myturn span').text('waiting... ('+(data.myturn?data.myturn:'..')+')');
      }
      confirm_btn_check();
    })
    var alerttt = 0;
    pisowifi.on('insertcoin-timer', function( data ){
      var seconds = data.time > 1? data.time + ' seconds':data.time + ' second';
      $('.wait-timer,.progress.insert-coin').css({visibility:'visible'})
      $('#insertcoin .seconds-timer').text(seconds)
      $('.progress.insert-coin .progress-bar').width(((data.time/data.max)*100) + '%');
      
      if (data['coindrop']) {
        coindrop.play();
      }
      if (data['added-time']) {
        coindrop.stop();
        addtime.play();
        alerttt = 0;
        var template = "<div class='alert alert-success text-left fade show' role='alert'>Success: Added <strong>" + (data['added-time'] / 60) + "</strong> minutes to your time.</div>";
        $('.coin-result').append(template);
      }
      if (data.time <= 1) {
        $('#insertcoin').modal('hide')
      }
      alerttt++;
      if (alerttt > 2) {
        $('.coin-result').children().first().alert('close')
      }
    })
    
    pisowifi.on('submit-voucher', function( data ){
      var type = data.result == 'error'? 'danger': data.result;
      data.result = data.result.charAt(0).toUpperCase() + data.result.slice(1);
      var voucher = 'Server Error';
      if (type == 'success') {
        if ( data.status == 0 ) {
          voucher = "Time: +<strong>" + data.time.toHHMMSS() + "</strong>";
        } else {
          type = 'danger';
          data.result = 'Error'
          voucher = "Used Voucher";
        }
        $('.voucher-code').val('');
      }
      var template = "<div class='alert alert-" + type + " text-left' role='alert'>" + data.result + "<br/>" + voucher + "</div>";
      $('.voucher-result').html(template);
    })
    
    $(document).on('click','.btn-insert-coin', function(e){
      setTimeout(function() {
        $('#insertcoin').modal('show');
      }, relayDelay );
      $(this).text('Please wait...')
      return false;
    })
    
    $('#insertcoin').on('show.bs.modal', function (e) {
      var data = {
        request: true,
        event: 'insertcoin'
      }
      $('.wait-timer,.progress.insert-coin').css({visibility:'hidden'})
      pisowifi.trigger( 'plugin.events', [data] );
    })
    $('#insertcoin').on('hide.bs.modal', function (e) {
      var data = {
        request: false,
        event: 'insertcoin'
      }
      pisowifi.trigger( 'plugin.events', [data] );
      $('.progress.insert-coin .progress-bar').width('100%');
      $(".alert").alert('close');
      window.location.reload(true);
    })
    
    $(document).on('click', '.btn-connect', function(){
      var data = {request: $(this).text() == 'Connect'};
      pisowifi.trigger( 'wifi-request', [data] );
    })
    $(document).on('input', '.voucher-code', function(){
      $('.btn-voucher').prop('disabled', $.trim(this.value).length <= 4 )
    })
    $(document).on('click', '.btn-voucher', function(){
      var data = { 
        event: 'submit-voucher',
        code: $.trim($('.voucher-code').val())
      };
      $(".alert").alert('close')
      var btn = $(this).prop('disabled', true ).data('text', $(this).text())
      var counter = 3;
      var tt3, tt;
      clearInterval(tt);
      tt = setInterval(function(){
        btn.text(function(){
          return $(this).data('text') + ' (' + ( counter-- ) + ')';
        });
        btn.prop('disabled', ( counter >= 0 ) || ($.trim($('.voucher-code').val()).length <= 4) )
        if (counter < 0) {
          clearInterval(tt);
          clearInterval(tt3);
          btn.text(btn.data('text'))
          tt3 = setTimeout(function(){
            $(".alert").alert('close')
          },5000)
        }
      },1000)
      pisowifi.trigger( 'plugin.events', [data] );
    })
    
	});
})( jQuery, window );